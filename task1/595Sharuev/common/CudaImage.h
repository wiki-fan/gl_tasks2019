#pragma once

#include "CudaResource.h"

template <typename T>
class CudaImage : public CudaResource<T>{
public:

	CudaImage(int _width, int _height, ResourceLocation _loc) : CudaResource<T>(_width * _height * sizeof(T), _loc), width(_width), height(_height) { }
	~CudaImage() { }

	int getWidth() const { return width; }
	int getHeight() const { return height; }

private:
	const int width = 0, height = 0;
};


