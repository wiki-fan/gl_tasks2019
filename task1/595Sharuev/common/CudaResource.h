#pragma once

#include <cuda_runtime_api.h>
#include "CudaError.h"

enum class ResourceLocation {
	Device,
	Host
};

template <typename T>
class CudaResource {
public:
    using ElementType = T;

	CudaResource(T* _handle, size_t _size, ResourceLocation _location) : location(_location), handle(_handle), size(_size) { }

	CudaResource(size_t _size, ResourceLocation _location) : location(_location), size(_size) {
		if (location == ResourceLocation::Device) {
			checkCudaErrors(cudaMalloc(reinterpret_cast<void**>(&handle), _size));
		}
		else {
			checkCudaErrors(cudaMallocHost(reinterpret_cast<void**>(&handle), _size));
		}
	}

	virtual ~CudaResource() {
		if (location == ResourceLocation::Device) {
			checkCudaErrors(cudaFree(handle));
		}
		else {
			checkCudaErrors(cudaFreeHost(handle));
		}
	}

	T* getHandle() const { return handle; }

	size_t getSize() const { return size; }

	ResourceLocation getLocation() const { return location; }

    void copyToSelf(const T* data){
        checkCudaErrors(cudaMemcpy(this->getHandle(), data, this->getSize(), cudaMemcpyHostToDevice));
    }

    void copyFromSelf(T* data) {
        checkCudaErrors(cudaMemcpy(this->getHandle(), data, this->getSize(), cudaMemcpyDeviceToHost));
    }

    void moveSelfTo(const ResourceLocation tgtLocation){
        if (location == tgtLocation) {
            return;
        }
        if (tgtLocation == ResourceLocation::Device) {
            T* devicePtr;
            checkCudaErrors(cudaMalloc((void**)&devicePtr, size));
            checkCudaErrors(cudaMemcpy(devicePtr, handle, size, cudaMemcpyHostToDevice));
            cudaFreeHost(handle);
            handle = devicePtr;
        }
        if (tgtLocation == ResourceLocation::Host) {
            T* hostPtr;
            checkCudaErrors(cudaMallocHost((void**)&hostPtr, size));
            checkCudaErrors(cudaMemcpy(hostPtr, handle, size, cudaMemcpyDeviceToHost));
            cudaFree(handle);
            handle = hostPtr;
        }
        location = tgtLocation;
    }

private:
	ResourceLocation location = ResourceLocation::Device;
	T *handle = nullptr;
	size_t size = 0;
};
