#include <common/CudaInfo.h>
#include <common/CudaResource.h>
#include <common/CudaImage.h>
#include <common/CudaTimeMeasurement.h>

#include <vector>
#include <memory>
//#include <driver_types.h>

#include "rasterizer.cuh"
#include <cudasoil/cuda_soil.h>

#include <assimp/Importer.hpp>      // C++ importer interface
#include <assimp/scene.h>           // Output data structure
#include <assimp/postprocess.h>     // Post processing flags
#include <SOIL2.h>

#include "Cuda1DResource.h"

#include <cuda_runtime_api.h>
#include <functional>

void makeMeasurementsPixelwise(const cudaDeviceProp& deviceProp, CudaTriangles& triangles, CudaColors& colors, CudaResource<glm::mat3x2>& bboxDevice) {
    struct TimeReport {
        float totalTime = 0.0f;
        size_t measurements = 0;
        glm::uvec2 blockSize;
    };
    std::vector<TimeReport> results;

    CudaImage<glm::u8vec3> dstImage(512,512, ResourceLocation::Device);

    CudaTimeMeasurement timer;
    auto runTask = [&](glm::uvec2 blockSize, size_t measureId ) {
        cudaMemset(dstImage.getHandle(), 0, dstImage.getSize());

        timer.start();
        rasterizePixelwise(&triangles, &colors, bboxDevice.getHandle(), &dstImage, blockSize);
        timer.stop();

        if (results.size() <= measureId) {
            results.resize(measureId + 1, {0.0f, 0, blockSize});
        }
        results[measureId].totalTime += timer.getSeconds();
        results[measureId].measurements++;
    };
    for (int measurement = 0; measurement < 10; measurement++) {
        size_t measureId = 0;

        for( unsigned int i = 4u; i < 7u; i++ ) {
            for( unsigned int j = 4u; j < 7u; j++ ) {
                glm::uvec2 blockSize = {1u << i, 1u << j};
                if( blockSize.x * blockSize.y > deviceProp.maxThreadsPerBlock ) {
                    break;
                }

                runTask( blockSize, measureId );

                ++measureId;
            }
        }
    }

    for( const auto& result : results ) {
        std::cout << "Block size\t" << result.blockSize.x << "x"
                  << result.blockSize.y << "\t\ttime " << result.totalTime / result.measurements << "s" << std::endl;
    }

}

void makeMeasurementsTrianglewise(const cudaDeviceProp deviceProp, CudaTriangles& triangles, CudaColors& colors, CudaResource<glm::mat3x2>& bboxDevice) {
    struct TimeReport {
        float totalTime = 0.0f;
        size_t measurements = 0;
        glm::u32 blockSize;
    };
    std::vector<TimeReport> results;

    CudaImage<glm::u8vec3> dstImage(512,512, ResourceLocation::Device);

    CudaTimeMeasurement timer;
    auto runTask = [&](glm::u32 blockSize, size_t measureId) {
        cudaMemset(dstImage.getHandle(), 0, dstImage.getSize());

        timer.start();
        rasterize_trianglewise(&triangles, &colors, bboxDevice.getHandle(), &dstImage, blockSize);
        timer.stop();

        if (results.size() <= measureId) {
            results.resize(measureId + 1, {0.0f, 0, blockSize});
        }
        results[measureId].totalTime += timer.getSeconds();
        results[measureId].measurements++;
    };

    for (int measurement = 0; measurement < 10; measurement++) {
        size_t measureId = 0;

        for( unsigned int i = 2u; i < 10u; i++ ) {
            glm::u32 blockSize = 1u << i;
            if( blockSize > deviceProp.maxThreadsPerBlock ) {
                break;
            }

            runTask( blockSize, measureId );

            ++measureId;
        }

    }

    for (const auto &result : results) {
        std::cout << "Block size\t" << result.blockSize << "\t\ttime " << result.totalTime / result.measurements << "s" << std::endl;
    }
}

int main() {
    Assimp::Importer importer;
    std::string pFile = "300_polygon_sphere_100mm.STL";
    const aiScene* scene = importer.ReadFile( pFile,
                                              aiProcess_CalcTangentSpace       |
                                              aiProcess_Triangulate            |
                                              aiProcess_JoinIdenticalVertices  |
                                              aiProcess_SortByPType);

    std::cout << scene->mNumMeshes << " " << scene->mNumMaterials <<std::endl;

    aiMesh* mesh = scene->mMeshes[0];
    CudaTriangles triangles(mesh->mNumFaces, ResourceLocation::Host);
    for (int i = 0; i<triangles.getN(); ++i) {
        for(int j = 0; j<3; ++j) {
            aiVector3D v = mesh->mVertices[mesh->mFaces[i].mIndices[j]];
            triangles.getHandle()[i][j].x = v.x;
            triangles.getHandle()[i][j].y = v.y;
            triangles.getHandle()[i][j].z = v.z;
        }
    }

    CudaResource<glm::mat3x2> bboxDevice(1, ResourceLocation::Device);
    init_bbox(&triangles, bboxDevice.getHandle());
    CudaColors colors(triangles.getN(), ResourceLocation::Device);
    init_colors(&triangles, bboxDevice.getHandle(), &colors);

    printCudaInfo();
    cudaDeviceProp deviceProp;
    cudaGetDeviceProperties(&deviceProp, 0);

    CudaImage<glm::u8vec3> dstImage1(512,512, ResourceLocation::Device);
    cudaMemset(dstImage1.getHandle(), 0, dstImage1.getSize());
    rasterize_trianglewise(&triangles, &colors, bboxDevice.getHandle(), &dstImage1, 512);
    dstImage1.moveSelfTo(ResourceLocation::Host);
    std::string path1 = "trianglewise.png";
    SOIL_save_image(path1.c_str(), SOIL_SAVE_TYPE_PNG, dstImage1.getWidth(), dstImage1.getHeight(), 3, (unsigned char*)dstImage1.getHandle());
    std::cout << "save image to " << path1 << ": " << SOIL_last_result() << std::endl;

    CudaImage<glm::u8vec3> dstImage2(512,512, ResourceLocation::Device);
    cudaMemset(dstImage2.getHandle(), 0, dstImage2.getSize());
    rasterizePixelwise(&triangles, &colors, bboxDevice.getHandle(), &dstImage2, glm::uvec2(32, 32));
    dstImage1.moveSelfTo(ResourceLocation::Host);
    std::string path2 = "pixelwise.png";
    SOIL_save_image(path2.c_str(), SOIL_SAVE_TYPE_PNG, dstImage1.getWidth(), dstImage1.getHeight(), 3, (unsigned char*)dstImage1.getHandle());
    std::cout << "save image to " << path2 << ": " << SOIL_last_result() << std::endl;

    makeMeasurementsPixelwise(deviceProp, triangles, colors, bboxDevice);
    makeMeasurementsTrianglewise(deviceProp, triangles, colors, bboxDevice);

    return 0;
}
