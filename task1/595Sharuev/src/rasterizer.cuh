#pragma once

#include <glm/glm.hpp>
#include "Cuda1DResource.h"
#include <common/CudaImage.h>

using CudaTriangles = Cuda1DResource<glm::mat3x3>;
using CudaColors = Cuda1DResource<glm::mat3x3>;

void init_bbox(CudaTriangles* triangles, glm::mat3x2* bboxDevice);
void init_colors(CudaTriangles* triangles, glm::mat3x2* bboxDevice, CudaColors* colors);

void rasterizePixelwise( CudaTriangles* triangles, CudaColors* colors, glm::mat3x2* bboxDevice, CudaImage <glm::u8vec3>* dst_image, glm::uvec2 blockDim );
void rasterize_trianglewise( CudaTriangles* triangles, CudaColors* colors, glm::mat3x2* bboxDevice, CudaImage <glm::u8vec3>* dst_image, glm::u32 blockDim);
