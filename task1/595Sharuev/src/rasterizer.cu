#include "rasterizer.cuh"
#include <common/CudaError.h>
#include <limits>

/// BEGIN(COMMON)
__global__
void get_bbox( const glm::mat3x3* t, int n, glm::mat3x2* ret )
{
    float min_x, max_x, min_y, max_y, min_z, max_z;
    min_x = max_x = t[0][0][0];
    min_y = max_y = t[0][0][1];
    min_z = max_z = t[0][0][2];
    for( int i = 0; i < n; i++ ) {
        for( int j = 0; j < 3; ++j ) {
            if( t[i][j][0] < min_x ) { min_x = t[i][j][0]; }
            if( t[i][j][0] > max_x ) { max_x = t[i][j][0]; }
            if( t[i][j][1] < min_y ) { min_y = t[i][j][1]; }
            if( t[i][j][1] > max_y ) { max_y = t[i][j][1]; }
            if( t[i][j][2] < min_z ) { min_z = t[i][j][2]; }
            if( t[i][j][2] > max_z ) { max_z = t[i][j][2]; }
        }
    }
    (*ret)[0][0] = min_x;
    (*ret)[0][1] = max_x;
    (*ret)[1][0] = min_y;
    (*ret)[1][1] = max_y;
    (*ret)[2][0] = min_z;
    (*ret)[2][1] = max_z;
    printf("BBOX %f %f %f %f %f %f\n", (*ret)[0][0], (*ret)[0][1], (*ret)[1][0],(*ret)[1][1],(*ret)[2][0],(*ret)[2][1]);
}

__device__ void get_bbox_triangle(const glm::mat3x3& t, const glm::mat3x2& bbox, glm::mat3x2& b) {
    for (int axis = 0; axis<3; ++axis) {
        b[axis][0] = b[axis][1] = t[0][axis];
    }
    for( int vert = 0; vert < 3; ++vert ) {
        for (int axis = 0; axis<3; ++axis) {
            if( t[vert][axis] < b[axis][0] ) { b[axis][0] = t[vert][axis]; }
            if( t[vert][axis] > b[axis][1] ) { b[axis][1] = t[vert][axis]; }
        }
    }

    for( int axis = 0; axis < 3; ++axis ) {
        for (int i = 0; i<2;++i ) {
            b[axis][i] = ( b[axis][i] - bbox[axis][0] ) / ( bbox[axis][1] - bbox[axis][0] );
        }
    }
}

__device__
void get_barycentric_coordinates(const glm::vec3& p, const glm::mat3x3& t, glm::vec3& ans)
{
    glm::vec3 v0 = t[1] - t[0], v1 = t[2] - t[0], v2 = p - t[0];

    float d00 = glm::dot(v0, v0);
    float d01 = glm::dot(v0, v1);
    float d11 = glm::dot(v1, v1);
    float d20 = glm::dot(v2, v0);
    float d21 = glm::dot(v2, v1);

    float denom = d00 * d11 - d01 * d01;
    ans[1] = (d11 * d20 - d01 * d21) / denom;
    ans[2] = (d00 * d21 - d01 * d20) / denom;
    ans[0] = 1.0f - ans[1]- ans[2];
}

__device__
void interpolate_color(const glm::vec3& bc, const glm::mat3& color, glm::u8vec3& dst) {
    dst = (glm::u8vec3) ( bc[0] * color[0] * 255.f + bc[1] * color[1] * 255.f + bc[2] * color[2] * 255.f );
}

__global__ void fill_color(glm::mat3x3* colors, glm::mat3x3* triangles, int n, glm::mat3x2* bbox) {
    static int next = 1;
    for (int i = 0; i<n; ++i) {
        for (int j = 0; j<3; ++j) {
            next = next * 1103515245 + 12345;
            int channel = (unsigned int)(next/65536) % 3;
            int d = 2;
            colors[i][j][channel] = ((triangles[i][j][d]-(*bbox)[d][0])/((*bbox)[d][1]-(*bbox)[d][0]))-0.5;
            colors[i][j][(channel+1)%3] = 0;
            colors[i][j][(channel+2)%3] = 0;
        }
    }
}

__global__ void init_zbuffer(glm::int32* zBuffer, int n, int value) {
    for (int i = 0; i<n; ++i) {
        zBuffer[i] = value;
    }
}

__host__ void init_bbox(CudaTriangles* triangles, glm::mat3x2* bboxDevice)
{
    get_bbox << < 1, 1 >> > ( triangles->getHandle(), triangles->getN(), bboxDevice );
}

__host__ void init_colors(CudaTriangles* triangles, glm::mat3x2* bboxDevice, CudaColors* colors)
{
    fill_color << < 1, 1 >> > ( colors->getHandle(), triangles->getHandle(), triangles->getN(), bboxDevice );
}
// END(COMMON)


/// BEGIN(PIXELWISE)
__global__ void rasterizeKernelPixelwise( glm::mat3x3* triangles, int n, glm::mat3x3* colors,
                                          glm::u8vec3* dst_image, int w, int h, glm::mat3x2* bbox )
{
    int fieldW = blockDim.x*gridDim.x;
    int fieldH = blockDim.y*gridDim.y;

    int pointX = blockDim.x*blockIdx.x+threadIdx.x;
    int pointY = blockDim.y*blockIdx.y+threadIdx.y;

    if (pointX > w || pointY > h) {
        return;
    }

    int pointIndex = gridDim.x * blockDim.x * blockDim.y * blockIdx.y + gridDim.x * blockDim.x * threadIdx.y+ blockDim.x * blockIdx.x + threadIdx.x;
    float maxZ = -1000000;

    for(int i = 0; i<n; ++i) {
        glm::vec3 bc;
        glm::mat3 triangleXY = triangles[i];
        triangleXY[0][2] = 0;
        triangleXY[1][2] = 0;
        triangleXY[2][2] = 0;

        auto point = glm::vec3(
            (*bbox)[0][0]+(float) pointX / fieldW*((*bbox)[0][1]-(*bbox)[0][0]),
            (*bbox)[1][0] + (float) pointY / fieldH*((*bbox)[1][1]-(*bbox)[1][0]),
            0 );
        auto z = glm::vec3( triangles[i][0].z, triangles[i][1].z, triangles[i][2].z );
        get_barycentric_coordinates( point, triangleXY, bc );
        if( bc.x >= 0 && bc.y >= 0 && bc.z >= 0 ) {
            float curZ = glm::dot( z, bc );
            if( curZ > maxZ ) {
                maxZ = curZ;
                interpolate_color(bc, colors[i], dst_image[pointIndex]);
            }
        }
    }
}

// This is wrapper function for calling DEVICE _rasterize function from HOST.
void rasterizePixelwise( CudaTriangles* triangles, CudaColors* colors, glm::mat3x2* bboxDevice, CudaImage <glm::u8vec3>* dst_image, glm::uvec2 blockDim )
{

    dim3 blkDim(blockDim.x, blockDim.y);
    dim3 grdDim(dst_image->getWidth() / blkDim.x, dst_image->getHeight() / blkDim.y);
    rasterizeKernelPixelwise <<< blkDim, grdDim >>> (
        triangles->getHandle(), triangles->getN(),
        colors->getHandle(),
        dst_image->getHandle(), dst_image->getWidth(), dst_image->getHeight(), bboxDevice );

    checkCudaErrors( cudaGetLastError() );
}
/// END(PIXELWISE)


/// BEGIN(TRIANGLEWISE)
__global__
void rasterize_trianglewise_kernel1( glm::mat3x3* triangles, int n, glm::mat3x3* colors, glm::i32* zBuffer,
                                    glm::u8vec3* dst_image, int w, int h, glm::mat3x2* bbox )
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;

    glm::mat3x2 bbox_triangle;
    get_bbox_triangle( triangles[i], *bbox, bbox_triangle );

    for( int x = (int)(bbox_triangle[0][0]*w); x < (int)(bbox_triangle[0][1]*w); ++x ) {
        for( int y = (int)(bbox_triangle[1][0]*h); y < (int)(bbox_triangle[1][1]*h); ++y ) {
            glm::vec3 bc;
            auto pointInMesh = glm::vec3(
                ( *bbox )[0][0] + (float) x / w * ( ( *bbox )[0][1] - ( *bbox )[0][0] ),
                ( *bbox )[1][0] + (float) y / h * ( ( *bbox )[1][1] - ( *bbox )[1][0] ),
                0 );
            auto z = glm::vec3( triangles[i][0].z, triangles[i][1].z, triangles[i][2].z );
            glm::mat3 triangleXY = triangles[i];
            triangleXY[0][2] = 0;
            triangleXY[1][2] = 0;
            triangleXY[2][2] = 0;
            get_barycentric_coordinates( pointInMesh, triangleXY, bc );
            if( bc.x >= 0 && bc.y >= 0 && bc.z >= 0 ) {
                glm::i32 curZ =
                    (int) ( ( glm::dot( z, bc ) - ( *bbox )[2][0] ) / ( ( *bbox )[2][1] - ( *bbox )[2][0] )* 1000000 );
                atomicMax( &zBuffer[w * y + x], curZ );
            }
        }
    }

}
__global__
void rasterize_trianglewise_kernel2( glm::mat3x3* triangles, int n, glm::mat3x3* colors, glm::i32* zBuffer,
                                     glm::u8vec3* dst_image, int w, int h, glm::mat3x2* bbox )
{
    int i = blockIdx.x*blockDim.x+threadIdx.x;
    
    glm::mat3x2 bbox_triangle;
    get_bbox_triangle(triangles[i], *bbox, bbox_triangle);
    
    for( int x = bbox_triangle[0][0]*w; x < bbox_triangle[0][1]*w; ++x ) {
        for( int y = bbox_triangle[1][0]*h; y < bbox_triangle[1][1]*h; ++y ) {
            glm::vec3 bc;
            auto pointInMesh = glm::vec3(
                ( *bbox )[0][0] + (float) x / w * ( ( *bbox )[0][1] - ( *bbox )[0][0] ),
                ( *bbox )[1][0] + (float) y / h * ( ( *bbox )[1][1] - ( *bbox )[1][0] ),
                0 );
            auto z = glm::vec3( triangles[i][0].z, triangles[i][1].z, triangles[i][2].z );
            glm::mat3 triangleXY = triangles[i];
            triangleXY[0][2] = 0;
            triangleXY[1][2] = 0;
            triangleXY[2][2] = 0;
            get_barycentric_coordinates( pointInMesh, triangleXY, bc );
            if( bc.x >= 0 && bc.y >= 0 && bc.z >= 0 ) {
                glm::i32 curZ = (int)((glm::dot( z, bc )-( *bbox )[2][0])/( ( *bbox )[2][1] - ( *bbox )[2][0] )*1000000);
                if (curZ == zBuffer[w * y + x]) {
                    interpolate_color(bc, colors[i], dst_image[w * y + x]);
                }

            }
        }
    }
    
}

void rasterize_trianglewise( CudaTriangles* triangles, CudaColors* colors, glm::mat3x2* bboxDevice, CudaImage <glm::u8vec3>* dst_image, glm::u32 blockDim)
{
    glm::i32* zBuffer;
    checkCudaErrors(cudaMalloc(&zBuffer, dst_image->getWidth()*dst_image->getHeight()*sizeof(glm::i32)));
    init_zbuffer<<<1,1>>>(zBuffer, dst_image->getWidth()*dst_image->getHeight(), -200000000);

    dim3 blkDim(blockDim);
    dim3 grdDim(triangles->getN() / blkDim.x);
    rasterize_trianglewise_kernel1 <<< blkDim, grdDim >>> (
        triangles->getHandle(), triangles->getN(),
            colors->getHandle(), zBuffer,
            dst_image->getHandle(), dst_image->getWidth(), dst_image->getHeight(), bboxDevice );
    rasterize_trianglewise_kernel2 <<< blkDim, grdDim >>> (
        triangles->getHandle(), triangles->getN(),
            colors->getHandle(), zBuffer,
            dst_image->getHandle(), dst_image->getWidth(), dst_image->getHeight(), bboxDevice );

    checkCudaErrors(cudaFree(zBuffer));

    checkCudaErrors( cudaGetLastError() );
}
/// END(TRIANGLEWISE)
