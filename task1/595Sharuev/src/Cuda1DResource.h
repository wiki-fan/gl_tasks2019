#pragma once

#include <assimp/Importer.hpp>      // C++ importer interface
#include <assimp/scene.h>           // Output data structure
#include <assimp/postprocess.h>     // Post processing flags

#include <vector>

#include <common/CudaResource.h>

template<typename T>
class Cuda1DResource : public CudaResource<T>
{
 public:

    Cuda1DResource( size_t _n, ResourceLocation _loc )
        : CudaResource<T>( _n * sizeof( T ), _loc ), n( _n )
    {
    }

    ~Cuda1DResource() override = default;

    size_t getN() const
    {
        return n;
    }



 private:
    const size_t n = 0;
};
